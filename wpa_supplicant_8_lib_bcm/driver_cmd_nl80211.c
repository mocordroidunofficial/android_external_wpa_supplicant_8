/*
 * Driver interaction with extended Linux CFG8021
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Alternatively, this software may be distributed under the terms of BSD
 * license.
 *
 */

#include "hardware_legacy/driver_nl80211.h"
#include "wpa_supplicant_i.h"
#include "config.h"
#ifdef ANDROID
#include "android_drv.h"
#endif

#define MAX_WPSP2PIE_CMD_SIZE		512

typedef struct android_wifi_priv_cmd {
	char *buf;
	int used_len;
	int total_len;
} android_wifi_priv_cmd;

static int drv_errors = 0;


static int wl_enable_3wire(void);
static int wl_disable_3wire(void);


static void wpa_driver_send_hang_msg(struct wpa_driver_nl80211_data *drv)
{
	drv_errors++;
	if (drv_errors > DRV_NUMBER_SEQUENTIAL_ERRORS) {
		drv_errors = 0;
		wpa_msg(drv->ctx, MSG_INFO, WPA_EVENT_DRIVER_STATE "HANGED");
	}
}

int wpa_driver_nl80211_driver_cmd(void *priv, char *cmd, char *buf,
				  size_t buf_len )
{
	struct i802_bss *bss = priv;
	struct wpa_driver_nl80211_data *drv = bss->drv;
	struct ifreq ifr;
	android_wifi_priv_cmd priv_cmd;
	int ret = 0;

	if (os_strcasecmp(cmd, "STOP") == 0) {
		linux_set_iface_flags(drv->global->ioctl_sock, bss->ifname, 0);
		wpa_msg(drv->ctx, MSG_INFO, WPA_EVENT_DRIVER_STATE "STOPPED");
	} else if (os_strcasecmp(cmd, "START") == 0) {
		linux_set_iface_flags(drv->global->ioctl_sock, bss->ifname, 1);
		wpa_msg(drv->ctx, MSG_INFO, WPA_EVENT_DRIVER_STATE "STARTED");
	} else if (os_strcasecmp(cmd, "MACADDR") == 0) {
		u8 macaddr[ETH_ALEN] = {};

		ret = linux_get_ifhwaddr(drv->global->ioctl_sock, bss->ifname, macaddr);
		if (!ret)
			ret = os_snprintf(buf, buf_len,
					  "Macaddr = " MACSTR "\n", MAC2STR(macaddr));
	} else if (os_strcasecmp(cmd, "3WIRE-START") == 0) {

		wpa_printf(MSG_DEBUG, "%s: 3WIRE-START", __func__);
		wl_enable_3wire();
		wpa_printf(MSG_DEBUG, "%s: 3WIRE-START end", __func__);

	} else if (os_strcasecmp(cmd, "3WIRE-STOP") == 0) {

		wpa_printf(MSG_DEBUG, "%s: 3WIRE-STOP", __func__);
		wl_disable_3wire();
		wpa_printf(MSG_DEBUG, "%s: 3WIRE-STOP end", __func__);

	} else if (os_strncasecmp(cmd, "ENABLEWOWLAN", 12) == 0) {
		ret = wpa_driver_enable_wowlan(drv);
	} else { /* Use private command */
		os_memcpy(buf, cmd, strlen(cmd) + 1);
		memset(&ifr, 0, sizeof(ifr));
		memset(&priv_cmd, 0, sizeof(priv_cmd));
		os_strncpy(ifr.ifr_name, bss->ifname, IFNAMSIZ);

		priv_cmd.buf = buf;
		priv_cmd.used_len = buf_len;
		priv_cmd.total_len = buf_len;
		ifr.ifr_data = &priv_cmd;

		if ((ret = ioctl(drv->global->ioctl_sock, SIOCDEVPRIVATE + 1, &ifr)) < 0) {
			wpa_printf(MSG_ERROR, "%s: failed to issue private commands\n", __func__);
			wpa_driver_send_hang_msg(drv);
		} else {
			drv_errors = 0;
			ret = 0;
			if ((os_strcasecmp(cmd, "LINKSPEED") == 0) ||
			    (os_strcasecmp(cmd, "RSSI") == 0) ||
			    (os_strcasecmp(cmd, "GETBAND") == 0) ||
			    (os_strncasecmp(cmd, "WLS_BATCHING", 12) == 0))
				ret = strlen(buf);
			else if ((os_strncasecmp(cmd, "COUNTRY", 7) == 0) ||
				 (os_strncasecmp(cmd, "SETBAND", 7) == 0)){
				//yufeng.yang debug only, must be replaced by patch of brcm
				union wpa_event_data data;
				os_memset(&data, 0, sizeof(data));
				wpa_supplicant_event(drv->ctx,
					EVENT_CHANNEL_LIST_CHANGED, &data);
				//end
//				wpa_supplicant_event(drv->ctx,
//					EVENT_CHANNEL_LIST_CHANGED, NULL);
			}
			wpa_printf(MSG_DEBUG, "%s %s len = %d, %d", __func__, buf, ret, strlen(buf));
		}
	}
	return ret;
}

int wpa_driver_set_p2p_noa(void *priv, u8 count, int start, int duration)
{
	char buf[MAX_DRV_CMD_SIZE];

	memset(buf, 0, sizeof(buf));
	wpa_printf(MSG_DEBUG, "%s: Entry", __func__);
	snprintf(buf, sizeof(buf), "P2P_SET_NOA %d %d %d", count, start, duration);
	return wpa_driver_nl80211_driver_cmd(priv, buf, buf, strlen(buf)+1);
}

int wpa_driver_get_p2p_noa(void *priv, u8 *buf, size_t len)
{
	/* Return 0 till we handle p2p_presence request completely in the driver */
	return 0;
}

int wpa_driver_set_p2p_ps(void *priv, int legacy_ps, int opp_ps, int ctwindow)
{
	char buf[MAX_DRV_CMD_SIZE];

	memset(buf, 0, sizeof(buf));
	wpa_printf(MSG_DEBUG, "%s: Entry", __func__);
	snprintf(buf, sizeof(buf), "P2P_SET_PS %d %d %d", legacy_ps, opp_ps, ctwindow);
	return wpa_driver_nl80211_driver_cmd(priv, buf, buf, strlen(buf) + 1);
}

int wpa_driver_set_ap_wps_p2p_ie(void *priv, const struct wpabuf *beacon,
				 const struct wpabuf *proberesp,
				 const struct wpabuf *assocresp)
{
	char buf[MAX_WPSP2PIE_CMD_SIZE];
	struct wpabuf *ap_wps_p2p_ie = NULL;
	char *_cmd = "SET_AP_WPS_P2P_IE";
	char *pbuf;
	int ret = 0;
	int i;
	struct cmd_desc {
		int cmd;
		const struct wpabuf *src;
	} cmd_arr[] = {
		{0x1, beacon},
		{0x2, proberesp},
		{0x4, assocresp},
		{-1, NULL}
	};

	wpa_printf(MSG_DEBUG, "%s: Entry", __func__);
	for (i = 0; cmd_arr[i].cmd != -1; i++) {
		os_memset(buf, 0, sizeof(buf));
		pbuf = buf;
		pbuf += sprintf(pbuf, "%s %d", _cmd, cmd_arr[i].cmd);
		*pbuf++ = '\0';
		ap_wps_p2p_ie = cmd_arr[i].src ?
			wpabuf_dup(cmd_arr[i].src) : NULL;
		if (ap_wps_p2p_ie) {
			os_memcpy(pbuf, wpabuf_head(ap_wps_p2p_ie), wpabuf_len(ap_wps_p2p_ie));
			ret = wpa_driver_nl80211_driver_cmd(priv, buf, buf,
				strlen(_cmd) + 3 + wpabuf_len(ap_wps_p2p_ie));
			wpabuf_free(ap_wps_p2p_ie);
			if (ret < 0)
				break;
		}
	}

	return ret;
}


// For controling Wifi 3-wired  BEGIN
// From Broadcom demo code

#include <cutils/properties.h>

#include <fcntl.h>
//#include <linux/delay.h>
#include <sys/socket.h>


#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <linux/if_packet.h>
#include <errno.h>


#define network_card 	"wlan0"

#define WLC_GET_VAR 		262
#define WLC_SET_VAR 		263
#define WLC_GET_RSSI 		127
#define WLC_GET_CHANNEL		29

typedef unsigned char	uint8;
typedef	unsigned char	bool;


/* Linux network driver ioctl encoding */
typedef struct wl_ioctl {
	uint cmd;	/* common ioctl definition */
	char *buf;	/* pointer to user buffer */
	uint len;		/* length of user buffer */
	uint set;	/* get or set request (optional) */
	uint used;	/* bytes read or written (optional) */
	uint needed;	/* bytes needed (optional) */
} wl_ioctl_t;



static int
bcm_mkiovar(char *name, char *data, uint datalen, char *buf, uint buflen)
{
	uint len;

	len = strlen(name) + 1;
	if ((len + datalen) > buflen)
		return 0;

	strncpy(buf, name, buflen);

	/* append data onto the end of the name string */
	memcpy(&buf[len], data, datalen);
	len += datalen;

	return len;
}


static int
wl_ioctl(struct ifreq *ifr, int cmd, void *buf, int len, bool set)
{
	wl_ioctl_t ioc;
	int ret = 0;
	int s;

	if ((s = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		wpa_printf(MSG_ERROR, "%s: fail get socket\n", __func__);
		return -1;
	}


	ioc.cmd = cmd;
	ioc.buf = buf;
	ioc.len = len;
	ioc.set = set;
	ifr->ifr_data = (caddr_t) &ioc;

	//wpa_printf(MSG_DEBUG, "ioc len = %d, data = %08x, %08x, %08x, %08x\n",
	//		sizeof(wl_ioctl_t), ioc.cmd, *(&(ioc.cmd) + 1), *(&(ioc.cmd) + 2), *(&(ioc.cmd) + 3));

	//wpa_printf(MSG_DEBUG, "shaohua local_buf = %s, cmd = %d, len = %d, set = %d, used = %d, needed = %d\n",
	//	ioc.buf, ioc.cmd, ioc.len, ioc.set ? 1 : 0, ioc.used, ioc.needed);


	if ((ret = ioctl(s, SIOCDEVPRIVATE, ifr)) < 0) {
		wpa_printf(MSG_ERROR, "%s: fail SIOCDEVPRIVATE(%s)\n", __func__, strerror(errno));
	}

	close(s);
	return ret;
}


static int
wl_cmd_check_card(char *cmd, int cmd_op, char * buf, int buf_len, int *data, int data_len, int set)
{
	struct ifreq ifr;
	int ret;


	sprintf(ifr.ifr_name, "%s", network_card);

	bcm_mkiovar(cmd, data, data_len, buf, buf_len);

	ret =  wl_ioctl(&ifr, cmd_op, buf, buf_len, set);

	wpa_printf(MSG_DEBUG, "%s: get wl reply: %s, ret = %d \n",__func__, buf, ret);

	return ret;
}


// Enable 3-wire
// mws_coex_bitmap 0xffff
// mws_wlanrx_prot 1
// wl mws_lterx_prot 1
// wl mws_ltetx_adv 1200


static int wl_enable_3wire(void)
{
	char data_cmd[256];
	int cmd_op = 0;

	int data = 0;


	//1.1 mws_coex_bitmap 0xffff
	data = strtol("0xffff", NULL, 0);
	cmd_op = WLC_SET_VAR;
	wl_cmd_check_card("mws_coex_bitmap", cmd_op, data_cmd, 255, &data, 4, 1);


	//1.2 get to check is set sucessfully
	data = 0;
	cmd_op = WLC_GET_VAR;
	wl_cmd_check_card("mws_coex_bitmap", cmd_op, data_cmd, 255, NULL, 0, 0);
	wpa_printf(MSG_DEBUG, "mws_coex_bitmap = 0x%04x\n", ((int *)data_cmd)[0]);



	//2.1 mws_wlanrx_prot 1
	data = 1;
	cmd_op = WLC_SET_VAR;
	wl_cmd_check_card("mws_wlanrx_prot", cmd_op, data_cmd, 255, &data, 4, 1);


	//2.2 get to check is set sucessfully
	data = 0;
	cmd_op = WLC_GET_VAR;
	wl_cmd_check_card("mws_wlanrx_prot", cmd_op, data_cmd, 255, NULL, 0, 0);
	wpa_printf(MSG_DEBUG, "mws_wlanrx_prot = %d\n", ((int *)data_cmd)[0]);


	//3.1 wl mws_lterx_prot 1
	data = 1;
	cmd_op = WLC_SET_VAR;
	wl_cmd_check_card("mws_lterx_prot", cmd_op, data_cmd, 255, &data, 4, 1);


	//3.2 get to check is set sucessfully
	data = 0;
	cmd_op = WLC_GET_VAR;
	wl_cmd_check_card("mws_lterx_prot", cmd_op, data_cmd, 255, NULL, 0, 0);
	wpa_printf(MSG_DEBUG, "mws_lterx_prot = %d\n", ((int *)data_cmd)[0]);


	//4.1 wl mws_ltetx_adv 1200
	data = 1200; //strtol("1200", NULL, 0);
	cmd_op = WLC_SET_VAR;
	wl_cmd_check_card("mws_ltetx_adv", cmd_op, data_cmd, 255, &data, 4, 1);


	//4.2 get to check is set sucessfully
	data = 0;
	cmd_op = WLC_GET_VAR;
	wl_cmd_check_card("mws_ltetx_adv", cmd_op, data_cmd, 255, NULL, 0, 0);
	wpa_printf(MSG_DEBUG, "mws_ltetx_adv = %d\n", ((int *)data_cmd)[0]);


	return 0;

}


// Disable 3-Wire
// wl mws_coex_bitmap 0x0
static int wl_disable_3wire(void)
{
	char data_cmd[256];
	int cmd_op = 0;

	int data = 0;


	//1.1 mws_coex_bitmap 0x0
	data = 0; //strtol("0x0", NULL, 0);
	cmd_op = WLC_SET_VAR;
	wl_cmd_check_card("mws_coex_bitmap", cmd_op, data_cmd, 255, &data, 4, 1);


	//1.2 get to check is set sucessfully
	data = 0;
	cmd_op = WLC_GET_VAR;
	wl_cmd_check_card("mws_coex_bitmap", cmd_op, data_cmd, 255, NULL, 0, 0);
	wpa_printf(MSG_DEBUG, "mws_coex_bitmap = 0x%04x\n", ((int *)data_cmd)[0]);


	return 0;

}
// For controling Wifi 3-wired  END

int wpa_driver_enable_wowlan(struct wpa_driver_nl80211_data *drv)
{
	struct nl_msg *msg;
	int ret = -1;

	msg = nlmsg_alloc();
	if (!msg)
		return -1;

	genlmsg_put(msg, 0, 0, drv->global->nl80211_id, 0, 0,
		    NL80211_CMD_SET_WOWLAN, 0);

	NLA_PUT_U32(msg, NL80211_ATTR_IFINDEX, drv->ifindex);

	struct nlattr *nl_wowlan;
	nl_wowlan = nla_nest_start(msg, NL80211_ATTR_WOWLAN_TRIGGERS);
	if (!nl_wowlan)
		goto nla_put_failure;
	NLA_PUT_FLAG(msg, NL80211_WOWLAN_TRIG_ANY);
	nla_nest_end(msg, nl_wowlan);

	ret = send_and_recv_msgs(drv, msg, NULL, NULL);
	msg = NULL;
	if (ret < 0)
		wpa_printf(MSG_ERROR, "nl80211: Enable WOWLAN failed: %d", ret);
	else
		wpa_printf(MSG_INFO, "nl80211: Enable WOWLAN succeed");

nla_put_failure:
	nlmsg_free(msg);
	return ret;
}
